#!/bin/bash

POPCORN=${HOME}/rtl/popcorn/toolchain.llvm37

make clean
./setclass.sh A
make POPCORN=${POPCORN}

IMAGEDIR="${HOME}/rtl/popcorn/qemu-images"

mkdir x86 arm

sudo mount -o loop,offset=1048576 ${IMAGEDIR}/x86.img x86
sudo mount -o loop ${IMAGEDIR}/arm.img arm

for i in ep is bt cg cj; do
    sudo cp ${i}/${i}_{aarch64,x86-64} x86/home/popcorn
    sudo cp ${i}/${i}_{aarch64,x86-64} arm/home/popcorn
    sudo cp ${i}/${i}_x86-64 x86/home/popcorn/${i}
    sudo cp ${i}/${i}_aarch64 arm/home/popcorn/${i}
done

sudo umount x86 arm
sudo rm -r x86 arm
