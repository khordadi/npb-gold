#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "migrate.h"

#define N 50
#define NUM_THREADS 2

int data[NUM_THREADS];

void *do_it(void *t)
{
  int ret = 0;
  int i = (int)t;

  data[i] = 10;

  pthread_exit (&ret);
}

int
main (int argc, char *argv[])
{
  int i;
  pthread_t threads[NUM_THREADS];
  
  migrate(0, NULL, NULL);

  for (i = 0; i < NUM_THREADS; i++)
    {
      pthread_create(&threads[i], NULL, do_it, (void *)i);
    }

  for (i = 0; i < NUM_THREADS; i++)
    {
      int status = 0;
      pthread_join (threads[i], &status);
    }

  migrate(1, NULL, NULL);

  printf (">>> %d\n", data[0]);

  return data[0];
}
